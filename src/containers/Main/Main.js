import React, { Component } from 'react';
import styles from './Main.module.css';
import SearchItemList from '../../components/SearchItemList/SearchItemList';
import axios from '../../axios';
import MovieDetails from '../../components/MovieDetails/MovieDetails';
import Input from '../../components/Input/Input';
import WatchList from '../../components/WatchList/WatchList';
import Spinner from '../../components/UI/Spinner/Spinner';
import Button from '../../components/UI/Button/Button';

// main component used for managing state and passing it to appropriate components
export default class Main extends Component {
	state = {
		data: [],
		movieId: null,
		movie: null,
		searchValue: '',
		watchlist: [],
		listIsLoading: false,
		movieIsLoading: false,
		errorModal: false
	};
	// loads movies saved to local storage to watchlist
	componentDidMount() {
		const watchlist = JSON.parse(localStorage.getItem('movies'));
		if (watchlist !== null) {
			this.setState({ watchlist });
		}
	}
	// gets details for a movie from RapidAPI, using movies id
	showMovieDetails = (id) => {
		this.setState({ movieIsLoading: true });
		axios.get(`&i=${id}`).then((res) => {
			this.setState({ movie: res.data, movieIsLoading: false });
		});
	};
	// handles input change event
	onChange = (event) => {
		const searchValue = event.target.value;
		this.setState({ searchValue });
	};
	// searches for a list of movies using user input
	onSearch = () => {
		this.setState({ listIsLoading: true });
		axios
			.get(`&s=${this.state.searchValue}`)
			.then((res) => {
				const data = res.data.Search;
				this.setState({ data, listIsLoading: false });
			})
			.catch(() => {
				this.setState({ listIsLoading: false });
			});
	};
	// adds movie to watchlist, and saves it to local storage
	onAddToWatchList = (title) => {
		const watchlist = [
			...this.state.watchlist
		];
		watchlist.push(title);
		localStorage.setItem('movies', JSON.stringify(watchlist));
		this.setState({ watchlist });
	};

	// removes item from watchlist and from local storage
	onRemove = (title) => {
		localStorage.removeItem('movies');
		const watchlist = this.state.watchlist.filter((wathclistTitle) => wathclistTitle !== title);
		this.setState({ watchlist });
		localStorage.setItem('movies', JSON.stringify(watchlist));
	};

	render() {
		// placeholder div for movie details
		let movieDetails = <div style={{ display: 'flex', flexBasis: '60%' }} />;

		let movieList;

		// displays list of movies using search query
		if (typeof this.state.data !== 'undefined') {
			movieList = <SearchItemList data={this.state.data} getId={this.showMovieDetails} search={this.onSearch} />;
		} else {
			// displays an error message in case of error
			movieList = <p style={{ color: 'white', textAlign: 'center', marginTop: '20px' }}>Check your spelling</p>;
		}

		if (this.state.listIsLoading) {
			movieList = <Spinner />;
		}

		if (this.state.movieIsLoading) {
			movieDetails = (
				<div style={{ display: 'flex', flexBasis: '60%', alignItems: 'center' }}>
					<Spinner />
				</div>
			);
		}

		// displays movie details
		if (this.state.movie !== null) {
			movieDetails = <MovieDetails movie={this.state.movie} watchlist={this.onAddToWatchList} />;
		}

		return (
			<div className={styles.Main}>
				<div className={styles.SelectionDiv}>
					<Input value={this.state.searchValue} getInput={this.onChange} placeholder="Input search query" />
					<Button click={this.onSearch} text="Search" />
					{movieList}
				</div>
				{movieDetails}
				<WatchList movieList={this.state.watchlist} remove={this.onRemove} />
			</div>
		);
	}
}
