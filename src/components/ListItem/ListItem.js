import React from 'react';
import styles from './ListItem.modul.css';

// component for rendering individual list item
const listItem = (props) => (
	<li className={styles.ListItem} onClick={props.click}>
		{props.text}
	</li>
);

export default listItem;
