import React from 'react';
import styles from './MovieDetails.module.css';
import Button from '../UI/Button/Button';

// component for rendering details for a specific movie
const movieDetails = (props) => (
	<div className={styles.MovieDetails}>
		<h2>Title: {props.movie.Title}</h2>
		<div className={styles.MovieData}>
			<div className={styles.ImgDiv}>
				<img src={props.movie.Poster} alt="img" />
			</div>
			<div className={styles.DetailsDiv}>
				<p>
					<strong>Released:</strong> {props.movie.Released}
				</p>
				<p>
					<strong>Runtime:</strong> {props.movie.Runtime}
				</p>
				<p>
					<strong>Genre:</strong> {props.movie.Genre}
				</p>
				<p>
					<strong>Director:</strong> {props.movie.Director}
				</p>
				<p>
					<strong>Actors:</strong> {props.movie.Actors}
				</p>
				<p>
					<strong>Country:</strong> {props.movie.Country}, <strong>language:</strong> {props.movie.Language}
				</p>
				<p>
					<strong>Production:</strong> {props.movie.Production}
				</p>
			</div>
		</div>
		<p>
			<strong>Plot:</strong> {props.movie.Plot}
		</p>
		<Button click={() => props.watchlist(props.movie.Title)} text="Add to watchlist" />
	</div>
);

export default movieDetails;
