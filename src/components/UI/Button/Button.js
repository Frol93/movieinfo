import React from 'react';
import styles from './Button.module.css';

const button = (props) => (
	<button className={styles.Button} onClick={props.click}>
		{props.text}
	</button>
);

export default button;
