import React from 'react';
import styles from './WatchList.module.css';
import ListItem from '../ListItem/ListItem';

// component for displaying a list of movies added to the watchlist
const watchList = (props) => {
	const titles = props.movieList.map((title) => {
		return <ListItem key={title} text={title} click={() => props.remove(title)} />;
	});

	return (
		<div className={styles.WatchList}>
			<p>Movies To Watch:</p>
			<ol>{titles}</ol>
		</div>
	);
};

export default watchList;
