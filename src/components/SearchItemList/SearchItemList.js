import React from 'react';
import ListItem from '../ListItem/ListItem';
import styles from './SearchItemList.module.css';

// component for rendering a list of result using data from props
const searchItemList = (props) => {
	const data = props.data.map((el) => {
		return <ListItem key={el.imdbID} text={el.Title} getId={props.getId} click={() => props.getId(el.imdbID)} />;
	});

	return (
		<div className={styles.SearchItemList}>
			<ol>{data}</ol>
		</div>
	);
};

export default searchItemList;
