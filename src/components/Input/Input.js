import React from 'react';
import styles from './Input.module.css';

// component for rendering input
const input = (props) => (
	<input
		className={styles.Input}
		type="text"
		value={props.value}
		onChange={(event) => props.getInput(event)}
		placeholder={props.placeholder}
	/>
);

export default input;
