import axios from 'axios';

// custom axios instance with a base url and appropriate headers
const instance = axios.create({
	baseURL: `https://movie-database-imdb-alternative.p.rapidapi.com/?page=1&r=json`
});

instance.defaults.headers.common['X-RapidAPI-Host'] = 'movie-database-imdb-alternative.p.rapidapi.com';
instance.defaults.headers.common['X-RapidAPI-Key'] = '30faba26e2mshcd256ffea78eac6p1eb7e8jsn179e8c167b75';

export default instance;
